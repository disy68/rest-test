package com.example;

import com.example.model.Response;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.ClientHttpRequestExecution;
import org.springframework.http.client.ClientHttpRequestInterceptor;

import java.util.Collections;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class RestTestApplicationTests {

    @Autowired
    private TestRestTemplate template;

    @Test
    void notRestrictedIsOk() {
        ResponseEntity<Response> responseEntity = template.getForEntity("/not-restricted", Response.class);

        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
    }

    @Test
    void restrictedIsUnauthorized() {
        ResponseEntity<Response> responseEntity = template.getForEntity("/restricted", Response.class);

        assertEquals(HttpStatus.UNAUTHORIZED, responseEntity.getStatusCode());
    }

    @Test
    void restrictedWithAuthHeaderIsOk() {
        addAuthorizationHeader();

        ResponseEntity<Response> responseEntity = template.getForEntity("/restricted", Response.class);

        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
    }

    @Test
    void basicWithoutAuthIsUnauthorized() {
        ResponseEntity<Response> responseEntity = template.getForEntity("/basic", Response.class);

        assertEquals(HttpStatus.UNAUTHORIZED, responseEntity.getStatusCode());
    }

    @Test
    void basicWithAuthIsOk() {
        ResponseEntity<Response> responseEntity = template
                .withBasicAuth("user", "password")
                .getForEntity("/basic", Response.class);

        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
    }

    private void addAuthorizationHeader() {
        HttpHeaders headers = new HttpHeaders();
        headers.set("Authorization", "present :-)");

        ClientHttpRequestInterceptor interceptor =  (HttpRequest request, byte[] body, ClientHttpRequestExecution execution) -> {
            request.getHeaders().add("Authorization", "present :-)");
            return execution.execute(request, body);
        };

        template.getRestTemplate().setInterceptors(Collections.singletonList(interceptor));
    }
}
