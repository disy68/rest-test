package com.example.controller;

import com.example.model.Response;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SomethingController {

    @GetMapping("/restricted")
    public Response restricted() {
        return new Response("Cool under heavy security!");
    }

    @GetMapping("/not-restricted")
    public Response notRestricted() {
        return new Response("Cool and free!");
    }
}
