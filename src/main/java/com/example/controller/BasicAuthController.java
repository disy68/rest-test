package com.example.controller;

import com.example.model.Response;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class BasicAuthController {

    @GetMapping("/basic")
    public Response needsBasicAuth() {
        return new Response("With basic auth!");
    }
}
