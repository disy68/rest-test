package com.example.filter;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static java.util.Objects.isNull;

public class AuthenticationFilter implements Filter {

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        var httpRequest = (HttpServletRequest) request;
        var authentication = httpRequest.getHeader("Authorization");

        if (isNull(authentication)){
            var httpResponse = (HttpServletResponse) response;
            httpResponse.setStatus(401);
        } else {
            chain.doFilter(request, response);
        }
    }
}
